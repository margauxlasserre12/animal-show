package ecole_softway.animal_show;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import ecole_softway.animal_show.animaux.Cigogne;

public class TestCigogne {
	@Test
	public void Presentation() throws Exception {
		Cigogne cigogne = new Cigogne("Pattes longues");
		assertThat(cigogne.presenteToi(), is("Je suis une cigogne blanche et je m'appelle Pattes longues."));
	}
}
