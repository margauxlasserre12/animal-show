package ecole_softway.animal_show;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import ecole_softway.animal_show.animaux.Cameleon;
import ecole_softway.animal_show.animaux.Elephant;
import ecole_softway.animal_show.animaux.Grenouille;

public class TestElephant {
	@Test
	public void changerCouleurAnimalSaufGrenouilleEnLançantBoue() {
		Cameleon cameleon = new Cameleon("Bibi", "vert");
		new Elephant("Babar").lancerBoue(cameleon);
		assertThat(cameleon.getColor(), is("marron sale"));
	}

	@Test
	public void couleurGrenouilleDoitResterVerteEnLançantBoue() {
		Grenouille grenouille = new Grenouille("Kermitt");
		new Elephant("Babar").lancerBoue(grenouille);
		assertThat(grenouille.getColor(), is("verte"));
	}
}
