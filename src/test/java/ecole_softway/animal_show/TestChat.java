package ecole_softway.animal_show;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

import ecole_softway.animal_show.animaux.Chat;

public class TestChat {
	@Test
	public void presentationDuChatSansJouer() throws Exception {
		assertThat(new Chat("Garfield", "roux").presenteToi(), is("..."));
	}

	@Test
	public void presentationDuChatEnJouantAvec() throws Exception {
		Chat chat = new Chat("Garfield", "roux");
		chat.jouer();
		assertThat(chat.presenteToi(), is("Je suis un chat roux et je m'appelle Garfield."));
	}
}
