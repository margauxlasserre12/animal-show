package ecole_softway.animal_show;

import ecole_softway.animal_show.animaux.Cameleon;
import ecole_softway.animal_show.animaux.Chat;
import ecole_softway.animal_show.animaux.Cigogne;
import ecole_softway.animal_show.animaux.Elephant;
import ecole_softway.animal_show.animaux.Grenouille;

public class App {
	public static void main(String[] args) {

		// cigogne
		System.out.println("____________________________________________________");
		System.out.println("\n");
		Cigogne cigogne = new Cigogne("Pattes longues");

		System.out.println(cigogne.presenteToi());
		System.out.println("\n");
		// chat
		System.out.println("____________________________________________________");
		System.out.println("\n");
		Chat chat = new Chat("Garfield", "roux");

		System.out.println(chat.presenteToi());
		chat.jouer();
		System.out.println(chat.presenteToi());
		System.out.println(chat.presenteToi());
		System.out.println("\n");
		// Cameleon
		System.out.println("____________________________________________________");
		System.out.println("\n");
		Cameleon cameleon = new Cameleon("Bibi", "vert");

		System.out.println(cameleon.presenteToi());
		cameleon.jouer();
		System.out.println(cameleon.presenteToi());
		System.out.println("\n");
		// Grenouille
		System.out.println("____________________________________________________");
		System.out.println("\n");
		Grenouille grenouille = new Grenouille("Kermitt");
		System.out.println(grenouille.presenteToi());
		System.out.println("\n");

		// Elephant
		System.out.println("____________________________________________________");
		System.out.println("\n");
		Elephant babar = new Elephant("Babar");
		System.out.println(babar.presenteToi());
		babar.lancerBoue(cameleon);

		System.out.println(cameleon.presenteToi()); /* Doit renvoyer: "Je suis un caméléon marron sale et je m'appelle Bibi" */
		cameleon.jouer();
		System.out.println(cameleon.presenteToi());

		Elephant dumbo = new Elephant("Dumbo");

		System.out.println(dumbo.presenteToi());
		babar.lancerBoue(dumbo);
		System.out.println(dumbo.presenteToi());
		try {
			dumbo.jouer();
		} catch (Exception e) {
			// e.printStackTrace();
			System.err.println(e.getMessage() + "\n");
		}

		Cigogne pattescourtes = new Cigogne("Pattes courtes");
		System.out.println(pattescourtes.presenteToi());
		babar.lancerBoue(pattescourtes);
		System.out.println(pattescourtes.presenteToi());

		System.out.println(grenouille.presenteToi());
		babar.lancerBoue(grenouille);
		System.out.println(grenouille.presenteToi());

		System.out.println("____________________________________________________");

	}
}
