package ecole_softway.animal_show.animaux;

import service.Jouable;

public class Chat extends Animal implements Jouable {

	private boolean content = false;

	public Chat(String name, String color) {
		super(name, color);
	}

	@Override
	public String presenteToi() {

		if (content) {
			content = false;
			return "Je suis un " + this.toString() + " " + color + " et je m\'appelle " + name + ".";
		} else {
			return "...";
		}
	}

	@Override
	public void jouer() {
		content = true;
	}

	@Override
	public String toString() {
		return "chat";
	}
}
