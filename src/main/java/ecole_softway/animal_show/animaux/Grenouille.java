package ecole_softway.animal_show.animaux;

public class Grenouille extends Animal {

	private static final String color = "verte";

	public Grenouille(String name) {
		super(name);
	}

	@Override
	public String getColor() {
		return color;
	}

	@Override
	public String presenteToi() {
		return "Je suis une " + toString() + " " + color + " et je m\'appelle " + name + ".";
	}

	@Override
	public String toString() {
		return "grenouille";
	}

}
