package ecole_softway.animal_show.animaux;

public abstract class Animal {

	protected final String name;
	protected String color;

	public Animal(String name, String color) {
		this.name = name;
		this.color = color;
	}

	public Animal(String name) {
		this.name = name;
	}

	public abstract String presenteToi();

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

}
