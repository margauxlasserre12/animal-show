package ecole_softway.animal_show.animaux;

import static service.Couleur.randomCouleur;
import service.Jouable;

public class Cameleon extends Animal implements Jouable {

	public Cameleon(String name, String color) {
		super(name, color);
	}

	@Override
	public String presenteToi() {
		return "Je suis un " + toString() + " " + color + " et je m\'appelle " + name + ".";
	}

	@Override
	public void jouer() {
		if (!"marron sale".equals(color)) {
			String couleurInitiale = color;
			while (couleurInitiale == color) {
				setColor(randomCouleur());
			}
		}
	}

	@Override
	public String toString() {
		return "cameleon";
	}

}
