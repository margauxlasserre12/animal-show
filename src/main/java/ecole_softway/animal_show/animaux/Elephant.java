package ecole_softway.animal_show.animaux;

import service.Jouable;

public class Elephant extends Animal implements Jouable {

	// champs ou attributs
	private String color = "gris";

	// constructeurs
	public Elephant(String name) {
		super(name);
	}

	// getters et setters
	@Override
	public void setColor(String color) {
		this.color = color;
	}

	// méthodes personnalisées
	@Override
	public String presenteToi() {
		return ("Je suis un " + toString() + " " + color + " et je m\'appelle " + name + ".").toUpperCase();
	}

	@Override
	public void jouer() throws Exception {
		throw new Exception("Vous venez de vous faire écraser par un éléphant.");
	}

	public void lancerBoue(Animal animal) {
		if (!"grenouille".equals(animal.toString())) {
			// if (animal.getClass() != Grenouille.class) {
			animal.setColor("marron sale");
		}
	}

	// toString
	@Override
	public String toString() {
		return "élèphant";
	}
}
