package ecole_softway.animal_show.animaux;

public class Cigogne extends Animal {

	private String color = "blanche";

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	public Cigogne(String name) {
		super(name);
	}

	@Override
	public String presenteToi() {
		return "Je suis une " + toString() + " " + color + " et je m\'appelle " + name + ".";
	}

	@Override
	public String toString() {
		return "cigogne";
	}

}
