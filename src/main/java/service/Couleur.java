package service;

import java.util.Random;

public enum Couleur {

	VERT("vert"), JAUNE("jaune"), ROUGE("rouge"), ROSE("rose"), VIOLET("violet");

	private static Random rand = new Random();
	private static Couleur[] couleurs = values();

	public static String randomCouleur() {
		return couleurs[rand.nextInt(couleurs.length)].getColor();
	}

	// end of static declarations

	private String color;

	private Couleur(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

}
